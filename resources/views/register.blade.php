<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook | Buat Akun Baru</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ url('/home') }}" method="post">
      @csrf
      <label for="first-name">First name:</label>
      <br />
      <input type="text" class="first-name" name="first-name" />
      <br />
      <label for="last-name">Last name:</label>
      <br />
      <input type="text" class="last-name" name="last-name" />
      <p>Gender:</p>
      <input type="radio" name="gender" id="male" value="male" />
      <label for="male">Male</label>
      <br />
      <input type="radio" name="gender" id="female" value="female" />
      <label for="female">Female</label>
      <br />
      <input type="radio" name="gender" id="other" value="other" />
      <label for="other">Other</label>
      <p>Nationality:</p>
      <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="American">American</option>
        <option value="Japanese">Japanese</option>
      </select>
      <p>Language Spoken:</p>
      <input
        type="checkbox"
        name="language"
        id="bahasa"
        value="Bahasa Indonesia"
      />
      <label for="bahasa">Bahasa Indonesia</label>
      <br />
      <input type="checkbox" name="language" id="english" value="English" />
      <label for="english">English</label>
      <br />
      <input type="checkbox" name="language" id="other" value="Other" />
      <label for="other">Other</label>
      <p>Bio:</p>
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="simpan">
    </form>
  </body>
</html>
