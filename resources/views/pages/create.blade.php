@extends('adminlte.master');

@section('content')
<form role="form" action=" {{ route('post.store') }} " method="POST">
  @csrf
    <div class="card-body">
      <h3>Post a question</h3>
      <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', '') }}" placeholder="Insert title">
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="question">Question</label>
        <textarea type="text" class="form-control" id="question" name="question" placeholder="Insert question">{{ old('question', '') }}</textarea>
        @error('question')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection