<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuestionIdToAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            //add question ID & profile ID
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('profile_id');
            
            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('profile_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            //
            $table->dropForeign(['question_id']);
            $table->dropForeign(['profile_id']);
            $table->dropColumn(['question_id']);
            $table->dropColumn(['profile_id']);
        });
    }
}
