<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuestionIdToQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            //add right answer ID & profile ID
            $table->unsignedInteger('right_answer_id')->nullable();
            $table->unsignedInteger('profile_id');
            
            $table->foreign('right_answer_id')->references('id')->on('answers');
            $table->foreign('profile_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            //
            $table->dropForeign(['right_answer_id']);
            $table->dropForeign(['profile_id']);
            $table->dropColumn(['right_answer_id']);
            $table->dropColumn(['profile_id']);
        });
    }
}
