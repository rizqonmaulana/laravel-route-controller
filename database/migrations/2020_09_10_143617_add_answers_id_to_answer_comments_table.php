<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnswersIdToAnswerCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answer_comments', function (Blueprint $table) {
            $table->unsignedInteger('answer_id');
            $table->unsignedInteger('profile_id');
            
            $table->foreign('answer_id')->references('id')->on('answers');
            $table->foreign('profile_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer_comments', function (Blueprint $table) {
            //
            $table->dropForeign(['answer_id']);
            $table->dropForeign(['profile_id']);
            $table->dropColumn(['answer_id']);
            $table->dropColumn(['profile_id']);
        });
    }
}
